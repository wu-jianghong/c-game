/* 【例7-4】输入一个正整数n (1＜n≤10)，再输入n个整数，将它们存入数组a中。
（1）输出最小值和它所对应的下标。
（2）将最小值与第一个数交换，输出交换后的n个数。
 */

/* 找出数组的最小值和它所对应的下标 */
# include <stdio.h>
int main(void)
{
    int i, index, n;
    int a[10];    

    printf("Enter n: ");            	    /* 提示输入n */
    scanf("%d", &n);

    printf("Enter %d integers: ", n); 	    /* 提示输入n 个数 */
    for(i = 0; i < n; i++)  
        scanf("%d", &a[i]);
   /* 找最小值a[index] */
    index = 0;								/* 假设a[0]是最小值，即下标为0的元素最小 */
    for(i = 1; i < n; i++)  
        if(a[i] < a[index])					/* 如果 a[i] 比假设的最小值还小 */
            index = i;						/* 再假设 a[i] 是新的最小值，即下标为 i 的元素最小 */
    /* 输出最小值和对应的下标 */
    printf("min is %d\tsub is %d\n", a[index], index);   /* 第19行 */

    return 0;
}
